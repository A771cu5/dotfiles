#!/usr/bin/zsh

# Load environment variables
# Change these as needed before running install
source "./zsh/variables.zsh" > /dev/null

# Zprezto
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${DOTDIR}/zprezto"
ln -s "${DOTDIR}/zprezto" "${HOME}/.zprezto"
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
sed -i "s/theme 'sorin'/theme 'walters'/g" $DOTDIR/zprezto/runcoms/zpreztorc
echo "source $DOTDIR/zsh/variables.zsh" >> $DOTDIR/zprezto/runcoms/zshrc
echo "source $DOTDIR/zsh/aliases.zsh" >> $DOTDIR/zprezto/runcoms/zshrc
echo "source $DOTDIR/zsh/hooks.zsh" >> $DOTDIR/zprezto/runcoms/zshrc
echo "source $DOTDIR/zsh/functions.zsh" >> $DOTDIR/zprezto/runcoms/zshrc

# Tmux
ln -s "${DOTDIR}/tmux/tmux.conf" "${HOME}/.tmux.conf"

# Vim
git clone https://bitbucket.org/A771cu5/vim_config.git "${VIMDIR}"
git clone https://github.com/VundleVim/Vundle.vim $VIMDIR/bundle/Vundle.vim
vim -u $VIMDIR/bundles.vim +BundleInstall! +qa!
ln -s "${VIMDIR}/vimrc" "${HOME}/.vimrc"
ln -s "${VIMDIR}" "${HOME}/.vim"

# X
ln -s "${XDIR}/xinitrc" "${HOME}/.xinitrc"

# i3
mkdir -p "${HOME}/.config/i3"
ln -s "${XDIR}/i3/config" "${HOME}/.config/i3/config"

# Terminator
mkdir -p "${HOME}/.config/terminator"
ln -s "${XDIR}/terminator/config" "${HOME}/.config/terminator/config"
