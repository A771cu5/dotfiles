#!/usr/bin/zsh

# This is a nasty temporary cleanup, use with caution!!

rm -rf $DOTDIR/zprezto
rm -rf $HOME/.z*
rm $HOME/.tmux.conf
rm -rf $DOTDIR/vim
rm -rf $HOME/.vim*
rm $HOME/.xinitrc
rm $HOME/.config/i3/config
rm $HOME/.config/terminator/config
