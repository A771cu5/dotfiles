# System
alias rzsh="source ${HOME}/.zshrc > /dev/null && echo 'Zsh reloaded!'"
alias l="ls -lh"
alias ll="ls -lA"
alias cl="clear"
alias running='ps aux | grep'
alias update-mirrors="sudo reflector --verbose --country 'United States' -l 200 --sort rate --save /etc/pacman.d/mirrorlist"
alias top='htop'

# Tmux
alias tmux="tmux -2"

# Vim
alias v="vim"

# Git
alias gs="git status"
alias gal="git add -A && echo '[Git] Added all to staging...'"
alias gc="git commit"

# Ruby
alias be="bundle exec"
alias ber="bundle exec rspec"
