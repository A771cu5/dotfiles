start() {
  sudo systemctl start $1.service
}

stop() {
  sudo systemctl stop $1.service
}
