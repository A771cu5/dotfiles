# System
export DOTDIR=$HOME/.dotfiles
export VIMDIR=$DOTDIR/vim
export XDIR=$DOTDIR/x

export VISUAL=vim
export EDITOR="$VISUAL"

export PATH=$PATH:$DOTDIR/tools
export PATH=$PATH:/usr/local/node/bin

export RBENV_ROOT=/usr/local/rbenv
export PATH=$RBENV_ROOT/bin:$PATH

export PYENV_ROOT=/usr/local/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
